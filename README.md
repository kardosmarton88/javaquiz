Java console application for testing Java knowledge in a Quiz style.<br>
Original implementation can be found on: https://gitlab.com/tstorcz/programming2 <br>
This project just extends it for Java learning purposes.<br>

Questions are in Hungarian language!<br>
Feel free to add yours in any other language in the Data folder just<br> 
follow the structure, or not and rewrite the code ;-)<br><br>
Happy Quizzing