import java.util.Arrays;

public class Question {
    private String text;
    private String[] answers;
    private String correct;

    public String getText() {
        return text;
    }

    public String[] getAnswers() {
        return this.answers;
    }

    public String getCorrect() {
        return correct;
    }

    public Question(String text, String[] answers, String correct) {
        this.text = text;
        this.answers = answers;
        this.correct = correct;
    }

    public void showQuestion() {
        System.out.println("Question:   " + this.getText());
        System.out.println("Answers:    " + Arrays.toString(this.getAnswers()));
        //System.out.println("Correct:    " + this.getCorrect());
    }
}
