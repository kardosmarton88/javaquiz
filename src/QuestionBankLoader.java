import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

public class QuestionBankLoader {
    public static Question[] LoadQuestions(String fileName) throws FileNotFoundException {
        ArrayList<Question> questions = new ArrayList<>();

        File source = new File(fileName);
        Scanner fileReader = new Scanner(source);

        String text;
        String correct;

        int index = 0;
        while(fileReader.hasNextLine()) {
            String[] answers = new String[4];
            text = fileReader.nextLine();
            for(int i=0;i<4;i++) {
                answers[i] = fileReader.nextLine().trim();
            }
            correct = fileReader.nextLine().substring(7).trim();
            questions.add(new Question(text, answers, correct));

            index++;

        }
        fileReader.close();

        Question[] ret = new Question[questions.size()];
        for(int i=0;i<questions.size(); i++)
            ret[i] = questions.get(i);

        return ret;
    }
}
