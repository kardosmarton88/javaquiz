import java.io.FileNotFoundException;

public class Main {
    public static void main(String[] args) throws FileNotFoundException {
        /*
        Java console application for testing Java knowledge in a Quiz style.
        Original implementation can be found on: https://gitlab.com/tstorcz/programming2
        This project just extends it for Java learning purposes.
        For further information read he README.md file.
        */
        System.out.println("\n\n =====   Welcome to Java quiz!   =====");
        QuestionBankController qbc = new QuestionBankController();
        qbc.main();

        System.out.println("\nThank You for testing!");
        System.out.println("Have a nice day! Bye!");
    }
}