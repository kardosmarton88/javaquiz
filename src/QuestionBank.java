import java.util.Arrays;

public class QuestionBank {
    private Question[] questions;

    public void setQuestions(Question question, int index) {
        this.questions[index] = question;
    }

    public int getQuestionCount() {return questions.length;}
    public Question getQuestion(int index) {return questions[index];}

    public QuestionBank(int size) {
        this.questions = new Question[size];
    }

    public void addQuestion(Question newQuestion, int index) {
        setQuestions(newQuestion, index);
    }

    public void printQuestions() {
        System.out.println("===== Print all the questions and answers =====");
        for (Question question : this.questions) {
            toString(question);
        }
    }

    public void printOneQuestion(int index) {
        System.out.println("\n===== The " + (index+1) + "-th Q/A: =====");
        toString(this.questions[index]);
    }

    public void toString(Question question) {
        System.out.println("Question:            " + question.getText());
        System.out.println("Possible answers :   " + Arrays.toString(question.getAnswers()));
        System.out.println("Correct answer:      " + question.getCorrect());
    }

    public void askQuestion(int questionNumber) {
        Question actualQuestion = this.questions[questionNumber];
        System.out.println("Question (" + (questionNumber+1) + "):    " + actualQuestion.getText());
        for (int i = 0; i < actualQuestion.getAnswers().length; i++) {
            System.out.println(actualQuestion.getAnswers()[i]);
        }
    }

    public String getCorrectAnswerForQuestion(int questionNumber) {
        Question actualQuestion = this.getQuestion(questionNumber);
        return actualQuestion.getCorrect();
    }
}
