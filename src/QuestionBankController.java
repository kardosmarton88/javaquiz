import java.awt.*;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;

public class QuestionBankController {
    private char getUserAnswer() {
        Scanner consoleScanner = new Scanner(System.in);
        char userAnswer = 'A';

        System.out.print("Your answer: ");
        try {
            userAnswer = consoleScanner.nextLine().charAt(0);
            return Character.toUpperCase(userAnswer);
        } catch (Exception e) {
            System.out.println("Something went wrong during getting the user input. " +
                    "By default value will be used for answer: " + userAnswer);
            return Character.toUpperCase(userAnswer);
        }
    }

    private int setNumberOfQuestion(int maxAvailableQuestion) {
        int numberOfQuestions = 0;
        Scanner consoleScanner = new Scanner(System.in);

        do {
            System.out.println("How many Questions do you want to get total randomly from all the questions? (1-" + maxAvailableQuestion + "): ");
            try {
                numberOfQuestions = Integer.parseInt(consoleScanner.nextLine());
            } catch (Exception e) {
                System.out.println("Enter a valid number between 1 and " + maxAvailableQuestion);
            }
        } while (0 >= numberOfQuestions || numberOfQuestions > maxAvailableQuestion);
        return numberOfQuestions;
    }

    private int[] generateGivenAmountOfRandomNumber( int numberOfArrayElement, int maxValue) {
        Random rand = new Random();
        return rand.ints(0, maxValue).distinct().limit(numberOfArrayElement).toArray();
    }

    private void resultCalculation(char[] usersAnswer,
                                   char[] correctAnswers,
                                   int[] questionNumbers,
                                   QuestionBank bank) {
        System.out.println("\n ===== Final result and self check =====");

        int correctCounter = 0;
        int sumOfQuestions = correctAnswers.length;
        if (usersAnswer.length == correctAnswers.length
                && questionNumbers.length == correctAnswers.length) {
            for (int i = 0; i < sumOfQuestions; i++) {
                bank.printOneQuestion(questionNumbers[i]);
                System.out.println("Your answer was: " + usersAnswer[i]);
                if (usersAnswer[i] == correctAnswers[i]) {
                    System.out.println("CORRECT! :)\n");
                    correctCounter++;
                } else {
                    System.out.println("WRONG! :(\n");
                }
            }

            System.out.println("\n\n=====  SUMMARY  =====");
            System.out.println("Your answers:     " + Arrays.toString(usersAnswer));
            System.out.println("Correct answers:  " + Arrays.toString(correctAnswers));

            System.out.println(" Your points: " + correctCounter + " out of " + sumOfQuestions + ", which is "
                    + (float) correctCounter/correctAnswers.length*100  + " %\n\n");
        } else {
            System.out.println("Something went wrong!");
        }
    }

    private void examMode(Question[] questionList, QuestionBank bank) {
        System.out.println("""

                Strict exam mode has been selected. I like that self confidence Champ!
                Let's go!
                """);

        int numberOfQuestion = setNumberOfQuestion(questionList.length);
        int[] randomQuestionArray = generateGivenAmountOfRandomNumber(numberOfQuestion, questionList.length);
        char[] correctAnswer = new char[numberOfQuestion];
        char[] yourAnswer = new char[numberOfQuestion];

        for ( int i = 0; i < randomQuestionArray.length; i++) {
            System.out.println("\n\n" + (i+1) + ". question from (" + randomQuestionArray.length + ")");
            bank.askQuestion(randomQuestionArray[i]);
            yourAnswer[i] = getUserAnswer();
            correctAnswer[i] = bank.getCorrectAnswerForQuestion(randomQuestionArray[i]).charAt(0);
        }

        resultCalculation(yourAnswer, correctAnswer, randomQuestionArray, bank);
    }

    private void studyMode(Question[] questionList, QuestionBank bank) {
        System.out.println("""
                
                Excellent choice! You have just selected the Study mode.\s
                We will go through all the questions and answers together Champ!

                Inner study mode selection.
                  - Backwards (Press b): From the last question to the first. Show all the Q/A one by one.
                  - Straight (Press s or any): From beginning to the end. Show all the Q/A one by one.
                """);

        int numberOfQuestion = questionList.length;
        char[] correctAnswer = new char[numberOfQuestion];
        char[] yourAnswer = new char[numberOfQuestion];
        int[] questionNumberArray = new int[questionList.length];

        if (getUserAnswer() == 'B') {
            System.out.println("\nBackwards mode has been selected!");

            for (int i = numberOfQuestion-1; i >= 0; i-- ) {
                System.out.println("\n\n" + (i+1) + ". question from (" + numberOfQuestion+ ")");
                bank.askQuestion(i);
                // Looks wierd at first because the backwards array uploading will upload array from the end
                // yourAnswer[i] = getUserAnswer(); -> yourAnswer[37] -> [0,0,0,0,0,0...0,0,'B']
                // At the end the array will store the elements in exactly the same order as it would go from 0,1,2..
                yourAnswer[i] = getUserAnswer();
                correctAnswer[i] = bank.getCorrectAnswerForQuestion(i).charAt(0);
                System.out.println("Correct answer is: " + questionList[i].getCorrect());
            }
        } else {
            System.out.println("\nStraight mode has been selected!");

            for (int i = 0; i < questionList.length; i++) {
                System.out.println("\n\n" + (i + 1) + ". question from (" + numberOfQuestion + ")");
                bank.askQuestion(i);
                yourAnswer[i] = getUserAnswer();
                correctAnswer[i] = bank.getCorrectAnswerForQuestion(i).charAt(0);
                System.out.println("Correct answer is: " + questionList[i].getCorrect());
            }
        }
        resultCalculation(yourAnswer, correctAnswer, questionNumberArray, bank);
    }

    private void letTheFunBegin() throws FileNotFoundException {
        Question[] questionList = QuestionBankLoader.LoadQuestions("Data" + File.separator + "Questions_hun.txt");
        QuestionBank bank = new QuestionBank(questionList.length);

        int index = 0;
        for (Question q : questionList) {
            bank.setQuestions(q, index);
            index++;
        }

        System.out.println("Select mode!");
        System.out.println("Study mode (Press S): Go through all the available questions where the question is " +
                "asked then the correct answer is shown.");
        System.out.println("Exam mode (Press E): Provide the amount of questions to be asked and " +
                " answer the randomly asked questions. At the end you will receive the summary of the quiz");
        switch (getUserAnswer()) {
            case 'S':
                studyMode(questionList, bank);
                break;
            case 'E':
                examMode(questionList, bank);
            default:
                System.out.println("There is no such option. Try again.");
        }
    }

    public void main() throws FileNotFoundException {
        char doYouWantSomeMore = 'Y';
        do {
            letTheFunBegin();
            System.out.println("Do you want test again? (Yes/No):");
            doYouWantSomeMore = getUserAnswer();
        } while (doYouWantSomeMore != 'N');

    }

}
